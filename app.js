require("dotenv").config();
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path"),
  io = require('socket.io')(server);

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});

// Import routes of our app

var socketsRouter = require("./app/routes/sockets");
var handlerError = require("./app/routes/handler");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));



// Define routes using URL path
app.use("/", socketsRouter);
app.use(handlerError);

//user tracking
var numUsers = 0;

// testing message
var messages = [
  {numUsers: numUsers
  },{
    author: "Carlos",
    text: "Hola! que tal?"
  },{
    author: "Pepe",
    text: "Muy bien! y tu??"  
  },{
    author: "Paco",
    text: "Genial!"
}];


/*Socket functions */
io.on('connection', function(socket) {
  console.log('Un cliente se ha conectado',messages[0].numUsers += 1 );
  // send messages once client is connected
  socket.emit('messages', messages);

  //listen to new messages from chat
  socket.on('newMessage', (data) => {
    // monitoring new messages
    console.log("new message from app: "+ data.user + ": " + data.msg);
    // send message to the rest of the clients
    socket.broadcast.emit('newMessage', data)
  }); 
  /*
  socket.on('new-message', function(data) {
    messages.push(data);
  
    io.sockets.emit('messages', messages);
  });
  */
});



module.exports = app;
